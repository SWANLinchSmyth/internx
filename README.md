# @internx workspace

Built on top of [yarn workspaces](https://classic.yarnpkg.com/en/docs/workspaces/) this package allows you to test behavior of different packages.

**This is not a monorepo**, it's just a wrapper for comfortable development while each package still have it's own VCS history, PRs etc.

## TL;DR

```
git clone %this_repo% @internx
cd @internx
git submodule update --init

yarn
yarn @/start
```

## Setup in details

```
# preparation
# cloning into `@internx` because bitbucket dropped `@` for some reason :(
git clone %this_repo% @internx
cd @internx
git submodule update --init

# installing dependencies (you need to do that in @internx folder aka workspace root)
yarn

# compiling components
yarn workspace @internx/ix-component-library publish:npm
```

And you're ready for running it!

You can run scripts **from individual repos**:

```
cd ./workspaces/ui
yarn storybook
```

or right **from the workspace root** (specifying package name):

```
yarn workspace @internx/ix-component-library start
```

or **using custom scripts** mapped to individual ones:

```
yarn ui/build
```

or using special root script with help of `foreman`:

```
yarn @/start
```

### Tips

- if you want to start WEB standalone, use `yarn web/start`
- if you want to start UI standalone, use `yarn ui/start`
- if you want UI changes to be hot-reloaded inside running WEB, you need to run `yarn ui/build:watch`

## Limitations and existing issues

- running `yarn @/start` will run WEB using port 5000 (not 3002)
- yarn workspaces using one root `.lock` file which may cause errors when deploying UI or WEB independently.
